package ru.speedway.spring15.controller;


import io.swagger.annotations.Api;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.speedway.spring15.domain.Book;
import ru.speedway.spring15.domain.Comment;
import ru.speedway.spring15.service.BookService;


import java.util.List;

@RestController
@Api( description = "Book REST controller")
public class BookController {

    private final BookService bookService;

    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

    @Secured("ROLE_USER")
    @GetMapping("/api/v1/book")
    public List<Book> getList() {
        return bookService.getAll();
    }

    @Secured("ROLE_USER")
    @GetMapping("/api/v1/book/{id}/comments")
    public List<Comment> getComments(@PathVariable("id") Long id) {
        Book book = bookService.getOne(id);
        return bookService.getComments(book);
    }

    @Secured("ROLE_USER")
    @GetMapping("/api/v1/book/{id}")
    public Book get(@PathVariable("id") Long id) {
        return bookService.getOne(id);
    }

    @Secured("ROLE_USER")
    @PostMapping("/api/v1/book")
    public Book add(@RequestBody Book book) {
        return bookService.edit(book);
    }

    @Secured("ROLE_ADMIN")
    @PutMapping("/api/v1/book")
    public Book update(@RequestBody Book book) {
        return bookService.edit(book);
    }

    @Secured("ROLE_ADMIN")
    @DeleteMapping("/api/v1/book/{id}")
    public void delete(@PathVariable("id") Long id) {
        bookService.delete(id);
    }

}
