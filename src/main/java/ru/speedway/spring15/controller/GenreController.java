package ru.speedway.spring15.controller;

import io.swagger.annotations.Api;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.speedway.spring15.domain.Genre;
import ru.speedway.spring15.service.GenreService;

import java.util.List;

@RestController
@Api( description = "Genre REST controller")
public class GenreController {
    private final GenreService genreService;

    public GenreController(GenreService genreService) {
        this.genreService = genreService;
    }

    @GetMapping("/api/v1/genre")
    @Secured("ROLE_USER")
    public List<Genre> getList() {
        return genreService.getAll();
    }

    @GetMapping("/api/v1/genre/{id}")
    @Secured("ROLE_USER")
    public Genre get(@PathVariable("id") Long id) {
        return genreService.getOne(id);
    }
}
