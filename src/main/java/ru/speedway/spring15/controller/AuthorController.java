package ru.speedway.spring15.controller;

import io.swagger.annotations.Api;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.speedway.spring15.domain.Author;
import ru.speedway.spring15.service.AuthorService;

import java.util.List;

@RestController
@Api( description = "Author REST controller")
public class AuthorController {

    private final AuthorService authorService;

    public AuthorController(AuthorService authorService) {
        this.authorService = authorService;
    }

    @GetMapping("/api/v1/author")
    @Secured("ROLE_USER")
    public List<Author> getList() {
        return authorService.getAll();
    }

    @GetMapping("/api/v1/author/{id}")
    @Secured("ROLE_USER")
    public Author get(@PathVariable("id") Long id) {
        return authorService.getOne(id);
    }
}
