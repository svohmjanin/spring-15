package ru.speedway.spring15.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.speedway.spring15.domain.Role;
import ru.speedway.spring15.domain.User;
import ru.speedway.spring15.dto.AuthenticationRequestDto;
import ru.speedway.spring15.dto.RegistrationRequestDto;
import ru.speedway.spring15.security.jwt.JwtTokenProvider;
import ru.speedway.spring15.service.RoleService;
import ru.speedway.spring15.service.UserService;

import java.util.*;

@RestController
public class AuthenticationController {
    private final AuthenticationManager authenticationManager;

    private final JwtTokenProvider jwtTokenProvider;

    private final UserService userService;
    private final RoleService roleService;

    public AuthenticationController(AuthenticationManager authenticationManager, JwtTokenProvider jwtTokenProvider, UserService userService, RoleService roleService) {
        this.authenticationManager = authenticationManager;
        this.jwtTokenProvider = jwtTokenProvider;
        this.userService = userService;
        this.roleService = roleService;
    }

    @PostMapping("/api/v1/auth/login")
    public ResponseEntity<Map<String, Object>> login(@RequestBody AuthenticationRequestDto requestDto) {
        try {
            String username = requestDto.getUsername();
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, requestDto.getPassword()));
            User user = userService.findByUsername(username).orElseThrow(() -> new UsernameNotFoundException("User is not found"));

            String token = jwtTokenProvider.createToken(username, user.getRoles());

            Map<String, Object> response = new HashMap<>();
            response.put("username", username);
            response.put("token", token);

            return ResponseEntity.ok(response);
        } catch (AuthenticationException e) {
            throw new BadCredentialsException("Invalid username or password");
        }
    }

    @PostMapping("/api/v1/auth/registration")
    public ResponseEntity<Map<String, Object>> registration(@RequestBody RegistrationRequestDto requestDto) {
        try {
            String username = requestDto.getUsername();

            Role userRole = roleService.findByName(RoleService.ROLE_USER).orElseThrow(() -> new UsernameNotFoundException("User role is not found"));

            List<Role> roles = Collections.singletonList(userRole);
            User user = new User(username, requestDto.getPassword(), requestDto.getFirstName(), requestDto.getLastName(), requestDto.getEmail());
            user.setRoles(roles);

            userService.register(user);

            String token = jwtTokenProvider.createToken(username, user.getRoles());

            Map<String, Object> response = new HashMap<>();
            response.put("username", username);
            response.put("token", token);

            return ResponseEntity.ok(response);
        } catch (AuthenticationException e) {
            throw new BadCredentialsException("Invalid user registration");
        }
    }
}
