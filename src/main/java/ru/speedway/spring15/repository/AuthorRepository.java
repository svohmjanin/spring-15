package ru.speedway.spring15.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.speedway.spring15.domain.Author;

public interface AuthorRepository extends JpaRepository<Author, Long> {
}
