package ru.speedway.spring15.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.speedway.spring15.domain.Role;

import java.util.Optional;

public interface RoleRepository extends JpaRepository<Role, Long>{
    Optional<Role> findByName(String username);
}