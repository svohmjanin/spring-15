package ru.speedway.spring15.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.speedway.spring15.domain.User;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByUsername(String username);
}