package ru.speedway.spring15.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.speedway.spring15.domain.Book;
import ru.speedway.spring15.domain.Comment;

import java.util.List;

public interface CommentRepository extends JpaRepository<Comment, Long> {

    List<Comment> findByBook(Book book);
}
