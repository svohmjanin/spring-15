package ru.speedway.spring15.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.speedway.spring15.domain.Genre;

public interface GenreRepository extends JpaRepository<Genre, Long> {
}
