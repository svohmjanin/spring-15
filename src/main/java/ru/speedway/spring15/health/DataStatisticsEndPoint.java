package ru.speedway.spring15.health;

import org.springframework.boot.actuate.endpoint.annotation.Endpoint;
import org.springframework.boot.actuate.endpoint.annotation.ReadOperation;
import org.springframework.stereotype.Component;
import ru.speedway.spring15.service.AuthorService;
import ru.speedway.spring15.service.BookService;
import ru.speedway.spring15.service.GenreService;

import java.util.LinkedHashMap;
import java.util.Map;

@Component
@Endpoint(id="data-statistic")
public class DataStatisticsEndPoint {

    private final BookService bookService;
    private final AuthorService authorService;
    private final GenreService genreService;

    public DataStatisticsEndPoint(BookService bookService, AuthorService authorService, GenreService genreService) {
        this.bookService = bookService;
        this.authorService = authorService;
        this.genreService = genreService;
    }

    @ReadOperation
    public Map<String, Object> health() {
        Map<String, Object> details = new LinkedHashMap<>();

        details.put("Books", bookService.count());
        details.put("Authors", authorService.count());
        details.put("Genres", genreService.count());
        return details;
    }
}
