package ru.speedway.spring15.health;

import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;
import ru.speedway.spring15.service.BookService;

@Component
public class EmptyBookTableIndicator implements HealthIndicator {

    private final BookService bookService;

    public EmptyBookTableIndicator(BookService bookService) {
        this.bookService = bookService;
    }

    @Override
    public Health health() {

        String message_key = "Book table";

        if (!isDatabaseNotEmpty()) {
            return Health.down().withDetail(message_key, "Book table is empty").build();
        }
        return Health.up().withDetail(message_key, "Book table is not empty").build();
    }

    private Boolean isDatabaseNotEmpty() {
        try{
            if(bookService.count() > 0){
                return true;
            }

        }catch (Exception e){
            return false;
        }

        return false;
    }

}
