package ru.speedway.spring15.domain;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Set;

@Entity
@Table(name = "role")
@Getter
@Setter
@ToString(exclude="id")
@NoArgsConstructor
@RequiredArgsConstructor
public class Role extends BaseEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    @NonNull
    @Size(max = 250)
    private String name;

    @ManyToMany(mappedBy = "roles")
    private Set<User> users;
}