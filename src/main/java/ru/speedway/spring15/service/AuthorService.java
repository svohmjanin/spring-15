package ru.speedway.spring15.service;

import ru.speedway.spring15.domain.Author;

import java.util.List;

public interface AuthorService {
    List<Author> getAll();
    Author getOne(Long id);
    long count();
}
