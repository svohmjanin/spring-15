package ru.speedway.spring15.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import ru.speedway.spring15.domain.Book;
import ru.speedway.spring15.domain.Comment;
import ru.speedway.spring15.repository.BookRepository;
import ru.speedway.spring15.repository.CommentRepository;
import ru.speedway.spring15.service.BookService;

import java.util.List;

@Service
public class BookServiceImpl implements BookService {

    private final BookRepository bookRepository;
    private final CommentRepository commentRepository;
    private static final Logger logger = LoggerFactory.getLogger(BookServiceImpl.class);

    public BookServiceImpl(BookRepository bookRepository, CommentRepository commentRepository) {
        this.bookRepository = bookRepository;
        this.commentRepository = commentRepository;
    }

    @Override
    public void delete(long id) {
        logger.info("BookService delete action by id: " + id);
        bookRepository.deleteById(id);
    }

    @Override
    public Book getOne(Long id) {
        logger.info("BookService getOne action by id: " + id);
        return bookRepository.getOne(id);
    }

    @Override
    public List<Comment> getComments(Book book){
        logger.info("BookService getComments action for book: " + book.getName());
        return commentRepository.findByBook(book);
    }

    @Override
    public Book edit(Book book) {
        logger.info("BookService edit action for book: " + book.getName());
        return bookRepository.save(book);
    }

    @Override
    public List<Book> getAll() {
        logger.info("BookService getAll action");
        return bookRepository.findAll();
    }

    @Override
    public long count() {
        logger.info("BookService get count");
        return bookRepository.count();
    }

}
