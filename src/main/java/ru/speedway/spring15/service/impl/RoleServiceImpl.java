package ru.speedway.spring15.service.impl;

import org.springframework.stereotype.Service;
import ru.speedway.spring15.domain.Role;
import ru.speedway.spring15.repository.RoleRepository;
import ru.speedway.spring15.service.RoleService;

import java.util.Optional;

@Service
public class RoleServiceImpl implements RoleService {

    private final RoleRepository roleRepository;

    public RoleServiceImpl(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @Override
    public Optional<Role> findByName(String name) {
        return roleRepository.findByName(name);
    }
}
