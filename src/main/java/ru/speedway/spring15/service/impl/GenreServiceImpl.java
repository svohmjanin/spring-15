package ru.speedway.spring15.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import ru.speedway.spring15.domain.Genre;
import ru.speedway.spring15.repository.GenreRepository;
import ru.speedway.spring15.service.GenreService;

import java.util.List;

@Service
public class GenreServiceImpl implements GenreService {

    private final GenreRepository genreRepository;
    private static final Logger logger = LoggerFactory.getLogger(GenreServiceImpl.class);

    public GenreServiceImpl(GenreRepository genreRepository) {
        this.genreRepository = genreRepository;
    }

    @Override
    public List<Genre> getAll() {
        logger.info("GenreService getAll action");
        return genreRepository.findAll();
    }

    @Override
    public Genre getOne(Long id) {
        logger.info("GenreService getOne action by id: " + id);
        return genreRepository.getOne(id);
    }

    @Override
    public long count() {
        logger.info("GenreService get count");
        return genreRepository.count();
    }
}
