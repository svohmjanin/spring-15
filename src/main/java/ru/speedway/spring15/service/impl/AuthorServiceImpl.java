package ru.speedway.spring15.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import ru.speedway.spring15.domain.Author;
import ru.speedway.spring15.repository.AuthorRepository;
import ru.speedway.spring15.service.AuthorService;

import java.util.List;

@Service
public class AuthorServiceImpl implements AuthorService {

    private final AuthorRepository authorRepository;
    private static final Logger logger = LoggerFactory.getLogger(AuthorServiceImpl.class);

    public AuthorServiceImpl(AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }

    @Override
    public List<Author> getAll() {
        logger.info("AuthorService getAll action");
        return authorRepository.findAll();
    }

    @Override
    public Author getOne(Long id) {
        logger.info("AuthorService getOne by id: " + id);
        return authorRepository.getOne(id);
    }

    @Override
    public long count() {
        logger.info("AuthorService get count");
        return authorRepository.count();
    }


}
