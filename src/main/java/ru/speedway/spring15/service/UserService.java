package ru.speedway.spring15.service;

import ru.speedway.spring15.domain.User;

import java.util.List;
import java.util.Optional;

public interface UserService {

    List<User> getAll();

    Optional<User> findByUsername(String username);

    Optional<User> findById(Long id);

    void delete(Long id);

    void register(User user);
}
