package ru.speedway.spring15.service;

import ru.speedway.spring15.domain.Genre;

import java.util.List;

public interface GenreService {
    List<Genre> getAll();
    Genre getOne(Long id);
    long count();
}
