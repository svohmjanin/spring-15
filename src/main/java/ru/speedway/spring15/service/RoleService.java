package ru.speedway.spring15.service;

import ru.speedway.spring15.domain.Role;

import java.util.Optional;

public interface RoleService {

    public static final String ROLE_USER = "ROLE_USER";
    public static final String ROLE_ADMIN = "ROLE_ADMIN";

    Optional<Role> findByName(String name);
}
