package ru.speedway.spring15.service;

import ru.speedway.spring15.domain.Book;
import ru.speedway.spring15.domain.Comment;

import java.util.List;

public interface BookService {
    void delete(long id);
    Book getOne(Long id);
    List<Comment> getComments(Book book);
    Book edit(Book book);
    List<Book> getAll();
    long count();
}
